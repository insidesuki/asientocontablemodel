<?php

namespace Insidesuki\Contabilidad\Domain\Exception;

use InvalidArgumentException;

class InvalidSubcuentaException extends InvalidArgumentException
{

	public function __construct(string $message,string $prefix)
	{
		parent::__construct(sprintf('La subcuenta %s, no tiene el prefijo correcto "%s"', $message,$prefix));
	}
}