<?php

namespace Insidesuki\Contabilidad\Domain\Exception;

use RuntimeException;

class CreatorAsientoDoesNotExistsException extends RuntimeException
{

	public function __construct(string $message)
	{
		parent::__construct(sprintf('No existe un creator para "%s"',$message));
	}
}