<?php

namespace Insidesuki\Contabilidad\Domain\Exception;

use InvalidArgumentException;

class InvalidFechaApunteException extends InvalidArgumentException
{

	public function __construct()
	{
		parent::__construct(sprintf('Invalid FechaApunte '));
	}
}