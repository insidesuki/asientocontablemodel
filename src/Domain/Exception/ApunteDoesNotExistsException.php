<?php

namespace Insidesuki\Contabilidad\Domain\Exception;

use RuntimeException;

class ApunteDoesNotExistsException extends RuntimeException
{

	public function __construct(string $message)
	{
		parent::__construct(sprintf('El apunte del tipo "%s", no existe en este asiento',$message));
	}

}