<?php

namespace Insidesuki\Contabilidad\Domain\Exception;

use Insidesuki\Contabilidad\Domain\Service\Factory\CreatorInterface;
use RuntimeException;

class InvalidCreatorException extends RuntimeException
{

	public function __construct(string $message)
	{
		parent::__construct(sprintf('El creator "%s" tiene que implementar la interface "%s"',
			$message, CreatorInterface::class));
	}
}