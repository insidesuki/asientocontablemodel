<?php

namespace Insidesuki\Contabilidad\Domain\Service\Asientos;

use Insidesuki\Contabilidad\Domain\Command\AsientoContableCommandInterface;
use Insidesuki\Contabilidad\Domain\Entity\AsientoContable;

abstract class AbstractCreateAsiento
{

	protected AsientoContable $asiento;

	public function __construct(
		protected AsientoContableCommandInterface $asientoCommand){
		$this->asiento =  AsientoContable::createAsiento($asientoCommand);
	}


}