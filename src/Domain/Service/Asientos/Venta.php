<?php

namespace Insidesuki\Contabilidad\Domain\Service\Asientos;

use Insidesuki\Contabilidad\Domain\Entity\AsientoContable;
use Insidesuki\Contabilidad\Domain\Service\Apuntes\ApunteCliente;
use Insidesuki\Contabilidad\Domain\Service\Apuntes\ApunteIva;
use Insidesuki\Contabilidad\Domain\Service\Apuntes\ApunteVenta;
use Insidesuki\Contabilidad\Domain\Service\Factory\CreatorInterface;

class Venta extends AbstractCreateAsiento implements CreatorInterface
{

	public function create(): AsientoContable
	{

		// apunteCliente
		$this->asiento->addApunte(
			new ApunteCliente(
				$this->asiento,
				$this->asientoCommand->getSubcuentaCliente(),
				$this->asientoCommand->getTotal(),
				$this->asientoCommand->getCodigoIva()
			)

		);

		// apunteVenta
		$this->asiento->addApunte(
			new ApunteVenta(
				$this->asiento,
				$this->asientoCommand->getSubcuentaVenta(),
				$this->asientoCommand->getBaseImponible()
			)
		);

		// apunteIva
		$this->asiento->addApunte(
			new ApunteIva(
				$this->asiento,
				$this->asientoCommand->getImporteIva()
			)
		);

		return $this->asiento;
	}


}