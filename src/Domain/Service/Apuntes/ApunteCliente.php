<?php

namespace Insidesuki\Contabilidad\Domain\Service\Apuntes;

use Insidesuki\Contabilidad\Domain\Entity\Apunte;
use Insidesuki\Contabilidad\Domain\Entity\AsientoContable;

class ApunteCliente extends Apunte
{

	public function __construct(
		AsientoContable $asientoContable,
		string $subcuenta,
		float $importe,
		int $codigoIva
	)
	{
		$this->checkSubcuenta($subcuenta,self::PREFIX_SUBCUENTA_CLIENTE);

		parent::__construct(
			$asientoContable,
			$subcuenta, $importe,
			parent::DEBE,
			1,
			parent::IVA_REPERCUTIDO,
			$codigoIva
		);
	}

}