<?php

namespace Insidesuki\Contabilidad\Domain\Service\Apuntes;

use Insidesuki\Contabilidad\Domain\Entity\Apunte;
use Insidesuki\Contabilidad\Domain\Entity\AsientoContable;

class ApunteIva extends Apunte
{

	public function __construct(
		AsientoContable $asientoContable,
		float $importe
	)
	{


		parent::__construct(
			$asientoContable,
			self::PREFIX_SUBCUENTA_IVA,
			$importe,
			parent::HABER,
			3
		);
	}

}