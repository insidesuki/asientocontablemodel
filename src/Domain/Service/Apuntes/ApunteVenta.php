<?php

namespace Insidesuki\Contabilidad\Domain\Service\Apuntes;

use Insidesuki\Contabilidad\Domain\Entity\Apunte;
use Insidesuki\Contabilidad\Domain\Entity\AsientoContable;

class ApunteVenta extends Apunte
{

	public function __construct(
		AsientoContable $asientoContable,
		string $subcuenta,
		float $importe
	)
	{

		// check numero de subcuenta
		$this->checkSubcuenta($subcuenta,self::PREFIX_SUBCUENTA_VENTA);

		parent::__construct(
			$asientoContable,
			$subcuenta,
			$importe,
			parent::DEBE,
			2,
			self::IVA_SOPORTADO
		);
	}

}