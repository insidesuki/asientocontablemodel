<?php

namespace Insidesuki\Contabilidad\Domain\Service\Factory;

use Insidesuki\Contabilidad\Domain\Entity\AsientoContable;

interface CreatorInterface
{
	public function create(): AsientoContable;
}