<?php

namespace Insidesuki\Contabilidad\Domain\Service\Factory;

use Insidesuki\Contabilidad\Domain\Command\AsientoContableCommandInterface;
use Insidesuki\Contabilidad\Domain\Entity\AsientoContable;
use Insidesuki\Contabilidad\Domain\Exception\CreatorAsientoDoesNotExistsException;
use Insidesuki\Contabilidad\Domain\Exception\InvalidCreatorException;

class AsientoFactory
{
	public static function create(string $asientoFqns, AsientoContableCommandInterface $cmd): AsientoContable
	{

		if(!class_exists($asientoFqns)){
			throw new CreatorAsientoDoesNotExistsException($asientoFqns);
		}

		$checkInterface = class_implements($asientoFqns,0);

		if(!$checkInterface){
			throw new InvalidCreatorException($asientoFqns);
		}
		$interface = implode('',$checkInterface);
		if($interface !== CreatorInterface::class){
			throw new InvalidCreatorException($asientoFqns);
		}

		return (new $asientoFqns($cmd))->create();

	}

}