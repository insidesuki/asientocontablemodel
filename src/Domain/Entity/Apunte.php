<?php

namespace Insidesuki\Contabilidad\Domain\Entity;

use Insidesuki\Contabilidad\Domain\Exception\InvalidSubcuentaException;
use Symfony\Component\Uid\Uuid;

abstract class Apunte
{

	public const DEBE                     = 'D';
	public const HABER                    = 'H';
	public const IVA_REPERCUTIDO          = 'R';
	public const IVA_SOPORTADO            = 'S';
	public const PREFIX_SUBCUENTA_IVA     = '477';
	public const PREFIX_SUBCUENTA_VENTA   = '70';
	public const PREFIX_SUBCUENTA_CLIENTE = '430';


	protected        $idApunte;
	protected int    $ordenRegistro;
	protected string $subcuenta;
	protected float  $importe;
	protected string $debeHaber;
	protected string $tipoIva;
	protected int    $codigoIva;
	protected float  $porcentajeIva = 21;

	private AsientoContable $asiento;


	protected function __construct(
		AsientoContable $asientoContable,
		string $subcuenta,
		float $importe,
		string $debeHaber,
		int $ordenRegistro,
		string $tipoIva = '',
		int $codigoIva = 0

	)
	{
		$this->idApunte      = Uuid::v4()->toRfc4122();
		$this->asiento       = $asientoContable;
		$this->subcuenta     = $subcuenta;
		$this->importe       = $importe;
		$this->debeHaber     = $debeHaber;
		$this->ordenRegistro = $ordenRegistro;
		$this->tipoIva       = $tipoIva;
		$this->codigoIva     = $codigoIva;

	}


	public function cambiarIva(float $nuevoIva): void
	{
		$this->porcentajeIva = $nuevoIva;
	}

	/**
	 * @return int
	 */
	public function ordenRegistro(): int
	{
		return $this->ordenRegistro;
	}

	/**
	 * @return string
	 */
	public function subcuenta(): string
	{
		return $this->subcuenta;
	}

	/**
	 * @return float
	 */
	public function importe(): float
	{
		return $this->importe;
	}

	/**
	 * @return string
	 */
	public function debeHaber(): string
	{
		return $this->debeHaber;
	}

	/**
	 * @return string
	 */
	public function tipoIva(): string
	{
		return $this->tipoIva;
	}

	/**
	 * @return int
	 */
	public function codigoIva(): int
	{
		return $this->codigoIva;
	}


	protected function checkSubcuenta(string $subcuenta, string $prefixAccount)
	{


		$prefix = substr($subcuenta, 0, strlen($prefixAccount));

		if($prefix !== $prefixAccount) {
			throw new InvalidSubcuentaException($subcuenta, $prefixAccount);
		}


	}

	protected function importeDetails(float $importe): array
	{

		$importeIva    = ($importe * $this->porcentajeIva) / 100;
		$baseImponible = ($importe - $importeIva);

		return [
			'importeIva'    => $importeIva,
			'baseImponible' => $baseImponible
		];

	}


	public function idApunte(): string
	{
		return $this->idApunte;
	}

}