<?php

namespace Insidesuki\Contabilidad\Domain\Entity;

use DateTime;
use Insidesuki\Contabilidad\Domain\Command\AsientoContableCommandInterface;
use Insidesuki\Contabilidad\Domain\Exception\ApunteDoesNotExistsException;
use Insidesuki\Contabilidad\Domain\Exception\InvalidFechaApunteException;
use Symfony\Component\Uid\Uuid;

class AsientoContable
{

	public const DEFAULT_CURRENCY = 'E';

	private string   $idAsiento;
	private int      $diario;
	private DateTime $fechaApunte;
	private int      $numeroAsiento;
	private string   $moneda;
	private string   $concepto;
	private string   $documento;
	private int      $userCreacion;
	private DateTime $fechaCreacion;
	private int      $userModificacion;
	private DateTime $fechaModificacion;
	private array    $apuntes;

	private function __construct
	(
		int $diario,
		DateTime $fechaApunte,
		int $numeroAsiento,
		string $concepto,
		string $documento
	)
	{

		$this->idAsiento     = Uuid::v4()->toRfc4122();
		$this->diario        = $diario;
		$this->fechaApunte   = $fechaApunte;
		$this->numeroAsiento = $numeroAsiento;
		$this->moneda        = self::DEFAULT_CURRENCY;
		$this->concepto      = $concepto;
		$this->documento     = $documento;
		$this->userCreacion  = 1;
		$this->fechaCreacion = new DateTime();
	}


	public static function createAsiento(AsientoContableCommandInterface $createAsientoContableCommand): self
	{

		$fechaApunte = DateTime::createFromFormat('d-m-Y', $createAsientoContableCommand->getFechaApunte());

		if(false === $fechaApunte) {
			throw new InvalidFechaApunteException();
		}

		return new self(
			$createAsientoContableCommand->getDiario(),
			$fechaApunte,
			$createAsientoContableCommand->getNumeroAsiento(),
			$createAsientoContableCommand->getConcepto(),
			$createAsientoContableCommand->getDocumento()

		);


	}

	public function addApunte(Apunte $apunte): void
	{

		$this->apuntes[$apunte::class] = $apunte;
	}

	public function diario(): int
	{
		return $this->diario;
	}

	public function fechaApunte(): DateTime
	{
		return $this->fechaApunte;
	}

	public function numeroAsiento(): int
	{
		return $this->numeroAsiento;
	}

	public function moneda(): string
	{
		return $this->moneda;
	}

	public function concepto(): string
	{
		return $this->concepto;
	}

	public function documento(): string
	{
		return $this->documento;
	}

	public function apuntes(): array
	{
		return $this->apuntes;
	}

	public function apunte(string $tipo): Apunte
	{
		if(!array_key_exists($tipo, $this->apuntes)) {
			throw new ApunteDoesNotExistsException($tipo);
		}

		return $this->apuntes[$tipo];
	}

	public function idAsiento(): string
	{
		return $this->idAsiento;
	}


	public function fechaCreacion(): DateTime
	{
		return $this->fechaCreacion;
	}
}