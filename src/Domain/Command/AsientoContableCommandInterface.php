<?php

namespace Insidesuki\Contabilidad\Domain\Command;

interface AsientoContableCommandInterface
{

	public function getDiario(): int;

	public function setDiario(int $diario): void;

	public function getFechaApunte(): string;

	public function setFechaApunte(string $fechaApunte): void;

	public function getNumeroAsiento(): int;

	public function setNumeroAsiento(int $numeroAsiento): void;

	public function getConcepto(): string;

	public function setConcepto(string $concepto): void;

	public function getDocumento(): string;

	public function setDocumento(string $documento): void;

	public function getCodigoIva(): int;

	public function setCodigoIva(int $codIva): void;

	public function getSubcuentaCliente(): string;

	public function setSubcuentaCliente(string $subcuenta): void;

	public function getTotal(): float;

	public function setTotal(float $importe): void;

	public function getSubcuentaVenta(): string;

	public function setSubcuentaVenta(string $subcuentaVenta): void;

	public function getImporteIva(): float;

	public function setImporteIva(float $importeIva): void;

	public function getBaseImponible(): float;

	public function setBaseImponible(float $baseImponible): void;

}