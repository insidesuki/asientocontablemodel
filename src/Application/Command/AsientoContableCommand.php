<?php

namespace Insidesuki\Contabilidad\Application\Command;

use Insidesuki\Contabilidad\Domain\Command\AsientoContableCommandInterface;
use Insidesuki\DDDUtils\Domain\DtoSerializer;

class AsientoContableCommand extends DtoSerializer implements AsientoContableCommandInterface
{

	protected int    $diario;
	protected string $fechaApunte;
	protected int    $numeroAsiento;
	protected string $concepto;
	protected string $documento;
	protected int    $codigoIva;
	protected string $subcuentaCliente;
	protected string $subcuentaVenta;
	protected float  $total;
	protected float  $baseImponible;
	protected float  $importeIva;


	public function getSubcuentaVenta(): string
	{
		return $this->subcuentaVenta;
	}


	public function setSubcuentaVenta(string $subcuentaVenta): void
	{
		$this->subcuentaVenta = $subcuentaVenta;
	}


	public function getDiario(): int
	{
		return $this->diario;
	}

	public function setDiario(int $diario): void
	{
		$this->diario = $diario;
	}

	public function getFechaApunte(): string
	{
		return $this->fechaApunte;
	}

	public function setFechaApunte(string $fechaApunte): void
	{
		$this->fechaApunte = $fechaApunte;
	}

	public function getNumeroAsiento(): int
	{
		return $this->numeroAsiento;
	}

	public function setNumeroAsiento(int $numeroAsiento): void
	{
		$this->numeroAsiento = $numeroAsiento;
	}

	public function getConcepto(): string
	{
		return $this->concepto;
	}

	public function setConcepto(string $concepto): void
	{
		$this->concepto = $concepto;
	}

	public function getDocumento(): string
	{
		return $this->documento;
	}

	public function setDocumento(string $documento): void
	{
		$this->documento = $documento;
	}

	public function getCodigoIva(): int
	{
		return $this->codigoIva;
	}

	public function setCodigoIva(int $codIva): void
	{
		$this->codigoIva = $codIva;
	}

	public function getSubcuentaCliente(): string
	{
		return $this->subcuentaCliente;
	}

	public function setSubcuentaCliente(string $subcuenta): void
	{
		$this->subcuentaCliente = $subcuenta;
	}

	public function getTotal(): float
	{
		return $this->total;
	}

	public function setTotal(float $total): void
	{
		$this->total = $total;
	}


	public function getBaseImponible(): float
	{
		return $this->baseImponible;
	}


	public function setBaseImponible(float $baseImponible): void
	{
		$this->baseImponible = $baseImponible;
	}


	public function getImporteIva(): float
	{
		return $this->importeIva;
	}

	public function setImporteIva(float $importeIva): void
	{
		$this->importeIva = $importeIva;
	}


}