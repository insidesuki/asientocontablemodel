<?php

namespace Domain\Service;

use Insidesuki\Contabilidad\Application\Command\AsientoContableCommand;
use Insidesuki\Contabilidad\Domain\Entity\AsientoContable;
use Insidesuki\Contabilidad\Domain\Exception\ApunteDoesNotExistsException;
use Insidesuki\Contabilidad\Domain\Service\Apuntes\ApunteCliente;
use Insidesuki\Contabilidad\Domain\Service\Apuntes\ApunteCobro;
use Insidesuki\Contabilidad\Domain\Service\Apuntes\ApunteIva;
use Insidesuki\Contabilidad\Domain\Service\Apuntes\ApunteVenta;
use Insidesuki\Contabilidad\Domain\Service\Asientos\Venta;
use PHPUnit\Framework\TestCase;

class CrearAsientoVentaTest extends TestCase
{

	public function setUp(): void
	{
		$this->cmd = new AsientoContableCommand();
		$this->cmd->setDiario(1);
		$this->cmd->setConcepto('venta factura 787878');
		$this->cmd->setDocumento('787878');
		$this->cmd->setNumeroAsiento(1);
		$this->cmd->setFechaApunte(date('d-m-Y'));
		$this->cmd->setSubcuentaCliente('4309090');
		$this->cmd->setSubcuentaVenta('701232');
		$this->cmd->setTotal(100.0);
		$this->cmd->setCodigoIva(12);
		$this->cmd->setImporteIva(21.00);
		$this->cmd->setBaseImponible(79);
	}


	public function testAsientoVentaWasCreated(){

		$asientoVentaService = new Venta($this->cmd);
		$asientoVenta = $asientoVentaService->create();
		$apuntes = $asientoVenta->apuntes();
		$this->assertInstanceOf(AsientoContable::class,$asientoVenta);
		$this->assertEquals(3,count($asientoVenta->apuntes()));
		$this->assertArrayHasKey(ApunteCliente::class,$apuntes);
		$this->assertArrayHasKey(ApunteVenta::class,$apuntes);
		$this->assertArrayHasKey(ApunteIva::class,$apuntes);


	}


	public function testApunteQuery(){
		$asientoVentaService = new Venta($this->cmd);
		$asientoVenta = $asientoVentaService->create();


		$apunte = $asientoVenta->apunte(ApunteCliente::class);
		$this->assertInstanceOf(ApunteCliente::class,$apunte);

	}

	public function testFailApunteDoesNotExists(){

		$this->expectException(ApunteDoesNotExistsException::class);
		$asientoVentaService = new Venta($this->cmd);
		$asientoVenta = $asientoVentaService->create();
		$apunteCobro = $asientoVenta->apunte(ApunteCobro::class);

	}




}
