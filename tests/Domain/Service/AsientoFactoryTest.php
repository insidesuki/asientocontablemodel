<?php

namespace Domain\Service;

use Insidesuki\Contabilidad\Application\Command\AsientoContableCommand;
use Insidesuki\Contabilidad\Domain\Entity\AsientoContable;
use Insidesuki\Contabilidad\Domain\Exception\CreatorAsientoDoesNotExistsException;
use Insidesuki\Contabilidad\Domain\Exception\InvalidCreatorException;
use Insidesuki\Contabilidad\Domain\Service\Asientos\Venta;
use Insidesuki\Contabilidad\Domain\Service\Factory\AsientoFactory;
use PHPUnit\Framework\TestCase;

class AsientoFactoryTest extends TestCase
{

	public function setUp(): void
	{
		$this->cmd = new AsientoContableCommand();
		$this->cmd->setDiario(1);
		$this->cmd->setConcepto('venta factura 787878');
		$this->cmd->setDocumento('787878');
		$this->cmd->setNumeroAsiento(1);
		$this->cmd->setFechaApunte(date('d-m-Y'));
		$this->cmd->setSubcuentaCliente('4309090');
		$this->cmd->setSubcuentaVenta('701232');
		$this->cmd->setTotal(100.0);
		$this->cmd->setCodigoIva(12);
		$this->cmd->setImporteIva(21.00);
		$this->cmd->setBaseImponible(79);
	}

	public function testAsientoWasCreated()
	{

		$asientoVenta = AsientoFactory::create(Venta::class, $this->cmd);
		$this->assertInstanceOf(AsientoContable::class,$asientoVenta);


	}

	public function testFailAsientoCreatorDoesNotExists(){

		$this->expectException(CreatorAsientoDoesNotExistsException::class);
		$asientoVenta = AsientoFactory::create(Otro::class, $this->cmd);

	}

	public function testFailInvalidAsientoCreator(){

		$this->expectException(InvalidCreatorException::class);
		$asientoVenta = AsientoFactory::create(DummyAsiento::class, $this->cmd);

	}

	public function testFailInvalidAsientoCreatorWithoutInterface(){

		$this->expectException(InvalidCreatorException::class);
		$asientoVenta = AsientoFactory::create(Dummies::class, $this->cmd);

	}

}

class Dummies {

}

class DummyAsiento implements dummyInterface
{


	public function test()
	{
		$a = 1;
	}
}

interface dummyInterface{

	public function test();

}
