<?php

namespace Domain\Entity;

use Insidesuki\Contabilidad\Application\Command\AsientoContableCommand;
use Insidesuki\Contabilidad\Domain\Entity\AsientoContable;
use PHPUnit\Framework\TestCase;

class AsientoContableTest extends TestCase
{

	public function setUp(): void
	{

		$this->cmd = new AsientoContableCommand();
		$this->cmd->setDiario(1);
		$this->cmd->setConcepto('venta factura 787878');
		$this->cmd->setDocumento('787878');
		$this->cmd->setNumeroAsiento(1);
		$this->cmd->setFechaApunte(date('d-m-Y'));
		$this->cmd->setSubcuentaCliente('4309999');
		$this->cmd->setSubcuentaVenta('701');
		$this->cmd->setTotal(100.0);
		$this->cmd->setCodigoIva(12);

	}

	public function testAsientoWasCreated(){

		$asientoContable = $this->getAsientoVenta();

		$this->assertInstanceOf(AsientoContable::class,$asientoContable);
		$this->assertSame($this->cmd->getDocumento(),$asientoContable->documento());
		$this->assertSame($this->cmd->getConcepto(),$asientoContable->concepto());
		$this->assertSame($this->cmd->getDiario(),$asientoContable->diario());
		$this->assertSame($this->cmd->getNumeroAsiento(),$asientoContable->numeroAsiento());
		$this->assertSame($this->cmd->getFechaApunte(),$asientoContable->fechaApunte()->format('d-m-Y'));
		$this->assertSame(AsientoContable::DEFAULT_CURRENCY,$asientoContable->moneda());

	}




	private function getAsientoVenta(): AsientoContable
	{

		return AsientoContable::createAsiento(
			$this->cmd
		);
	}

}
