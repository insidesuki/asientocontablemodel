<?php

namespace Domain\Entity\Apuntes;

use Insidesuki\Contabilidad\Application\Command\AsientoContableCommand;
use Insidesuki\Contabilidad\Domain\Entity\AsientoContable;
use Insidesuki\Contabilidad\Domain\Exception\InvalidSubcuentaException;
use Insidesuki\Contabilidad\Domain\Service\Apuntes\ApunteVenta;
use PHPUnit\Framework\TestCase;

class ApunteVentaTest extends TestCase
{

	public function setUp(): void
	{
		$this->cmd = new AsientoContableCommand();
		$this->cmd->setDiario(1);
		$this->cmd->setConcepto('venta factura 787878');
		$this->cmd->setDocumento('787878');
		$this->cmd->setNumeroAsiento(1);
		$this->cmd->setFechaApunte(date('d-m-Y'));
		$this->cmd->setSubcuentaCliente('4309090');
		$this->cmd->setSubcuentaVenta('701232');
		$this->cmd->setTotal(100.0);
		$this->cmd->setCodigoIva(12);
		$this->cmd->setBaseImponible(79.00);
		$this->cmd->setImporteIva(21.00);
	}

	public function testApunteVentaWasCreated(){

		$apunteVenta = new ApunteVenta(
			$this->getAsiento(),
			$this->cmd->getSubcuentaVenta(),
			$this->cmd->getBaseImponible()
		);


		$this->assertInstanceOf(ApunteVenta::class, $apunteVenta);
		$this->assertSame($this->cmd->getSubcuentaVenta(), $apunteVenta->subcuenta());
		$this->assertSame($this->cmd->getBaseImponible(), $apunteVenta->importe());
		$this->assertSame(2, $apunteVenta->ordenRegistro());
		$this->assertSame(ApunteVenta::DEBE, $apunteVenta->debeHaber());
		$this->assertSame(ApunteVenta::IVA_SOPORTADO, $apunteVenta->tipoIva());
		$this->assertEquals(0, $apunteVenta->codigoIva());

	}


	public function testFailInvalidSubcuenta()
	{

		$this->expectException(InvalidSubcuentaException::class);
		$apunteVenta = new ApunteVenta(
			$this->getAsiento(),
			'430kkk',
			$this->cmd->getTotal()
		);

	}

	private function getAsiento(): AsientoContable
	{

		return AsientoContable::createAsiento(
			$this->cmd
		);
	}

}
