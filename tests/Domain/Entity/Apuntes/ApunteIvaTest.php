<?php

namespace Domain\Entity\Apuntes;

use Insidesuki\Contabilidad\Application\Command\AsientoContableCommand;
use Insidesuki\Contabilidad\Domain\Entity\Apunte;
use Insidesuki\Contabilidad\Domain\Entity\AsientoContable;
use Insidesuki\Contabilidad\Domain\Service\Apuntes\ApunteIva;
use PHPUnit\Framework\TestCase;

class ApunteIvaTest extends TestCase
{


	public function setUp(): void
	{
		$this->cmd = new AsientoContableCommand();
		$this->cmd->setDiario(1);
		$this->cmd->setConcepto('venta factura 787878');
		$this->cmd->setDocumento('787878');
		$this->cmd->setNumeroAsiento(1);
		$this->cmd->setFechaApunte(date('d-m-Y'));
		$this->cmd->setSubcuentaCliente('4309090');
		$this->cmd->setSubcuentaVenta('701232');
		$this->cmd->setTotal(100.0);
		$this->cmd->setCodigoIva(12);
		$this->cmd->setBaseImponible(79.00);
		$this->cmd->setImporteIva(21.00);
	}


	public function testApunteIvaWasCreated(){

		$apunteIva = new ApunteIva(
			$this->getAsiento(),
			$this->cmd->getImporteIva()
		);


		$this->assertInstanceOf(ApunteIva::class,$apunteIva);
		$this->assertSame($apunteIva->subcuenta(),ApunteIva::PREFIX_SUBCUENTA_IVA);
		$this->assertSame(($this->cmd->getImporteIva()),$apunteIva->importe());
		$this->assertSame(3,$apunteIva->ordenRegistro());
		$this->assertSame(Apunte::HABER,$apunteIva->debeHaber());
		$this->assertEmpty($apunteIva->tipoIva());
		$this->assertEquals(0,$apunteIva->codigoIva());

	}


	private function getAsiento(): AsientoContable
	{

		return AsientoContable::createAsiento(
			$this->cmd
		);
	}
}
