<?php

namespace Domain\Entity\Apuntes;

use Insidesuki\Contabilidad\Application\Command\AsientoContableCommand;
use Insidesuki\Contabilidad\Domain\Entity\Apunte;
use Insidesuki\Contabilidad\Domain\Entity\AsientoContable;
use Insidesuki\Contabilidad\Domain\Exception\InvalidSubcuentaException;
use Insidesuki\Contabilidad\Domain\Service\Apuntes\ApunteCliente;
use PHPUnit\Framework\TestCase;

class ApunteClienteTest extends TestCase
{


	public function setUp(): void
	{

		$this->cmd = new AsientoContableCommand();
		$this->cmd->setDiario(1);
		$this->cmd->setConcepto('venta factura 787878');
		$this->cmd->setDocumento('787878');
		$this->cmd->setNumeroAsiento(1);
		$this->cmd->setFechaApunte(date('d-m-Y'));
		$this->cmd->setSubcuentaCliente('4309999');
		$this->cmd->setSubcuentaVenta('701');
		$this->cmd->setTotal(100.0);
		$this->cmd->setCodigoIva(12);
		$this->cmd->setBaseImponible(79.00);
		$this->cmd->setImporteIva(21.00);
	}


	public function testApunteClienteWasCreated(){

		$asiento = $this->getAsiento();
		$apunteCliente = new ApunteCliente(
			$asiento,
			$this->cmd->getSubcuentaCliente(),
			$this->cmd->getTotal(),
			$this->cmd->getCodigoIva()
		);

		$this->assertInstanceOf(ApunteCliente::class,$apunteCliente);
		$this->assertSame($this->cmd->getSubcuentaCliente(),$apunteCliente->subcuenta());
		$this->assertSame($this->cmd->getTotal(),$apunteCliente->importe());
		$this->assertSame(1,$apunteCliente->ordenRegistro());
		$this->assertSame(Apunte::DEBE,$apunteCliente->debeHaber());
		$this->assertSame(Apunte::IVA_REPERCUTIDO,$apunteCliente->tipoIva());
		$this->assertSame($this->cmd->getCodigoIva(),$apunteCliente->codigoIva());
	}

	public function testFailInvalidSubcuenta(){

		$this->expectException(InvalidSubcuentaException::class);
		$apunteCliente = new ApunteCliente(
			$this->getAsiento(),
			'420',
			$this->cmd->getTotal(),
			$this->cmd->getCodigoIva()
		);

	}




	private function getAsiento(): AsientoContable
	{

		return AsientoContable::createAsiento(
			$this->cmd
		);
	}


}
