<?php

use Insidesuki\Contabilidad\Application\Command\AsientoContableCommand;
use Insidesuki\Contabilidad\Domain\Service\Apuntes\ApunteCliente;
use Insidesuki\Contabilidad\Domain\Service\Asientos\Venta;
use Insidesuki\Contabilidad\Domain\Service\Factory\AsientoFactory;

require __DIR__ . '/../vendor/autoload.php';

try {
	$cmd = new AsientoContableCommand();
	$cmd->setDiario(1);
	$cmd->setConcepto('venta factura 787878');
	$cmd->setDocumento('787878');
	$cmd->setNumeroAsiento(1);
	$cmd->setFechaApunte(date('d-m-Y'));
	$cmd->setSubcuentaCliente('4309090');
	$cmd->setSubcuentaVenta('701232');
	$cmd->setTotal(100.0);
	$cmd->setCodigoIva(12);

	$asientoVenta = AsientoFactory::create(Venta::class, $cmd);
}
catch (Exception | RuntimeException $ex) {
	dd($ex->getMessage());
}
// asiento
dump($asientoVenta);

// apunte
//$apunteCliente = $asientoVenta->apunte(ApunteCliente::class);

//dd($apunteCliente);